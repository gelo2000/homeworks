package railwaystation;

import java.util.Timer;

/**
 * Организовать приложение касса вокзала.
 * Приложение должно позволять:
 * 1) Создавать рейс(количество вагонов, класс удобства(использовать enum),
 * город отбытия/прибытия(использовать enum), время в пути, скоростной/нескоростной).
 * 2) Выводить список всех рейсов.
 * 3) Выводить список рейсов по городу отбытия/прибытия.
 * 4) Просматривать отдельный рейс.
 * 5) Выводить рейсы, у которых вагоны типа люкс/эконом.
 */
public class Cashbox {
    private int vagon;
    private int timeInWay;
    private ClassOfVagon typeOfVagon;
    private City cityOfSend;
    private City cityOfGet;
    private SpeedLimit speedControl;
    private Cashbox array;


    public Cashbox(int vagon, int timeInWay, ClassOfVagon typeOfVagon,
                   City cityOfSend, City cityOfGet, SpeedLimit speedControl) {
        this.vagon = vagon;
        this.timeInWay = timeInWay;
        this.typeOfVagon = typeOfVagon;
        this.cityOfSend = cityOfSend;
        this.cityOfGet = cityOfGet;
        this.speedControl = speedControl;

    }

    public City getCityOfGet() {
        return cityOfGet;
    }

    public City getCityOfSend() {
        return cityOfSend;
    }

    public ClassOfVagon getTypeOfVagon() {
        return typeOfVagon;
    }
}

class CashBoxService {
    private Cashbox[] races;

    public CashBoxService() {
        this.races = new Cashbox[10];
    }

    public void addRace(Cashbox nameOfRace) {
        for (int i = 0; i < races.length; i++) {
            if (races[i] == null) {          ///???
                races[i] = nameOfRace;
                System.out.println(races[i]);
                break;                       ////???
            }
        }
    }

    public void showRaceByCity(City town) {
        for (Cashbox way : races) {
            if (town.equals(way.getCityOfGet()) || town.equals(way.getCityOfSend())) {
                System.out.println(way);
            }
        }
    }

    public void nameOfRace(Cashbox nameRace) {
        for (Cashbox name : races) {
            if (nameRace.equals(name)) {
                System.out.println(name);
            }
        }
    }

    public void classOfVagon(ClassOfVagon type) {
        for (Cashbox object : races) {                    ////????
            if (type.equals(object.getTypeOfVagon())) {
                System.out.println(object);
            }
        }
    }

}


enum ClassOfVagon {
    ECONON, NORMAL, LUX
}

enum City {
    KYIV, DNIPRO, ZHITOMYR, LVIV, ODESA
}

enum SpeedLimit {
    FAST, SLOW
}

class Test {
    public static void main(String[] args) {

        Cashbox raceKyivDnepr = new Cashbox(10, 7, ClassOfVagon.LUX, City.KYIV, City.DNIPRO, SpeedLimit.FAST);
        Cashbox raceLvivKyiv = new Cashbox(7, 12, ClassOfVagon.ECONON, City.LVIV, City.KYIV, SpeedLimit.FAST);
        Cashbox raceZhitomyrOdesa = new Cashbox(4, 6, ClassOfVagon.NORMAL, City.ZHITOMYR, City.ODESA, SpeedLimit.SLOW);
        Cashbox raceZhitomyrDnepr = new Cashbox(12, 5, ClassOfVagon.LUX, City.ZHITOMYR, City.DNIPRO, SpeedLimit.FAST);

        CashBoxService link = new CashBoxService();

        link.addRace(raceKyivDnepr);
        link.addRace(raceLvivKyiv);
        link.addRace(raceZhitomyrOdesa);
        link.addRace(raceZhitomyrDnepr);

        link.showRaceByCity(City.DNIPRO);

        link.nameOfRace(raceKyivDnepr);

        link.classOfVagon(ClassOfVagon.LUX);
    }
}