package lessons.learn_enums;

/**
 * Created by oleg on 02.08.17.
 */
public enum City {
    DNEPR("Dnepr"), KIEV("Kiev"), ZP("Zp"), PAVLOGRAD("Pavlograd");

    private String shortName;

    private City(String name) {
        this.shortName = name;
    }

    public String getShortName() {
        return shortName;
    }
}

class Trip {

    /*public static void getTrip(String city) {//"DnepR, KEEV"

    }*/

    public void getRun() {
        getTrip(City.ZP);
    }

    public static void getTrip(City city) {//"DnepR, KEEV
    }

    public static void main(String[] args) {
        getTrip(City.ZP);
//        getTrip("ZP");

        City zp = City.ZP;

//        System.out.println(zp.getShortName());
        System.out.println(City.DNEPR.getShortName());
    }
}

