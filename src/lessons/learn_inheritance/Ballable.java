package lessons.learn_inheritance;

public interface Ballable {
    void run();
}
