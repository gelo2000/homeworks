package lessons.learn_inheritance;

public   interface Jumpable extends Ballable, Cloneable {
    void jump();
}
