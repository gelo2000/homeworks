package lessons.learn_inheritance;

public abstract class Car implements Drivable, Jumpable {
    private String model;//has-a
    private int age;

    public Car(String model, int age) {
        this.model = model;
        this.age = age;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public abstract void foo();

    @Override
    public void print() {
        System.out.println("Car");
    }

    @Override
    public void jump() {
        System.out.println("Jump");
    }

    @Override
    public void run() {
        System.out.println("Hello from Car");
    }

    @Override
    public void printNumber() {
        System.out.println("I'm Car");
    }
}


class Bmw extends Car {//is-a
    private String bmwSeries;

    public Bmw(String model, int age, String bmwSeries) {
        super(model, age);
//        super();
        this.bmwSeries = bmwSeries;
    }

    public String getBmwSeries() {

        return bmwSeries;
    }

    @Override
    public void foo() {
        System.out.println();
    }

    @Override
    public void run() {
        System.out.println("Hello from Bmw");
    }
}

class Mercedes extends Car {

    public Mercedes(String model, int age, String mercedesSeries) {
        super(model, age);
        this.mercedesSeries = mercedesSeries;
    }

    private String mercedesSeries;


    @Override
    public void foo() {

    }

    @Override
    public void run() {

    }
}

class TestInh {
    public static void main(String[] args) {
//        Car bmw = new Bmw("", 10, "");

//        Drivable bmw = new Bmw("", 10, "");

        Jumpable bmw = new Bmw("", 10, "");


//        bmw.getBmwSeries();
    }
}
