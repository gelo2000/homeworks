package lessons.learn_inheritance;

public interface Drivable {

    int NUMBER = 5;

    void print();

    default void printNumber() {
        System.out.println("Number");
    }
}
