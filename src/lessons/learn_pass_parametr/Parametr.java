package lessons.learn_pass_parametr;

public class Parametr {

    public void changeVariableInt(int a) {
        a = 77;
    }

    public void changeVariableInteger(Integer i) {
        i = 77;
    }

    public String changeVariableString(String str) {//copy reference on object "Hello"
        str = "77";//new String("77")
        //Integer e = 6;

        //e = 8;//new Integer(8)
        return str;
    }

    public void changeObjectOfChanger(Changer changer) {//changer is copy on "new Changer()" object
        changer.name = "Other name";
    }
}

class TestParametr {
    public static void main(String[] args) {
        Parametr parametr = new Parametr();

        int b = 10;

        parametr.changeVariableInt(b);// int copyB = 10; parametr.changeVariableInt(copyB);

//        System.out.println(b);
        String s = "Hello";

        s = "99";

//        s = parametr.changeVariableString(s);

//        System.out.println(s);

  /*      parametr.changeVariableString(s);//creating copy reference s

        System.out.println(s);*/

        Integer r = new Integer(8);

//        parametr.changeVariableInteger(r);

//        System.out.println(r);

        Changer ch = new Changer();

        ch.name = "World";

        parametr.changeObjectOfChanger(ch);//creating copy of ch

        System.out.println(ch.name);

    }
}

class TestChanger {
    public static void main(String[] args) {
        Address address = new Address();

        Changer changer = new Changer();

        Changer vasya;

        vasya = new Changer();

        changer.address = address;

        changer.address.city = "Kiev";

        Changer other = changer;

        other.address.city = "Dnepr";

        System.out.println(changer.address.city);

    }
}
