
package random;

import java.util.Random;

/**
 * 2) В одномерном массиве случайных чисел. Выяснить,
 * имеются ли в данном массиве два идущих подряд четных элемента.
 * Подсчитать количество таких пар.
 */


public class Rand {
    public static void main(String[] args) {
        Random random = new Random();

        int[] array = new int[10];

        int count = 0;

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(23);
        }

        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 + array[i + 1] % 2 == 0) {
                count++;
                System.out.print(array[i] + "\t" + array[i + 1]);
            }
            //           int[] mass=new int[array[i]];
            //          System.out.println(i);
        }

        System.out.println("Count = " + count);

    }
}
