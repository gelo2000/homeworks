package area_calculate;

public interface Calculatable {
    public void calculateSquare();
    default void print(){
        System.out.println(getClass());
    }
}
