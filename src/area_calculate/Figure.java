package area_calculate;

/*Линейный поиск, биаарный поиск, сортировка  quick sort*/

/*Создать интерфейс Calculatable с методом calculateSquare и default метод print,
        который печатает название фигуры. Создать абстрактный класс Figure с
        полями(длина стороны, высота), который реализует интерфейс Calculatable.
        Создать классы Triangle, Parallelogram, Romb. В классах наследниках переопределить
        метод calculateSquare, для расчета площади. Протестировать функционал.*/

public abstract class Figure implements Calculatable {
    private int sideLength;
    private int high;

    public Figure(int sideLength,int high){
        this.sideLength = sideLength;
        this.high = high;
    }
    @Override
    public void calculateSquare() {
        int area;
        area=sideLength*high;
        System.out.println(area+"sq.m.");
    }
    public void print(){
        System.out.println(getClass());
    }
}
