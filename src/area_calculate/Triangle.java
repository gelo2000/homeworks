package area_calculate;

public class Triangle extends Figure {

    public Triangle(int sideLength,int high){
        super(sideLength,high);
    }
    public void calculateSquare(int sideLength, int high) {
        double area;
        area = sideLength * high * 0.5;
        System.out.println(area + "sq.m.");
    }


}

class TestTriangle {
    public static void main(String[] args) {
        Triangle linkTriangle = new Triangle(4, 9);
        linkTriangle.calculateSquare();
        linkTriangle.print();
    }
}