package area_calculate;

public class Parallelogram extends Figure {

    public Parallelogram(int sideLength,int high){
        super(sideLength, high);
    }
}
class TestParallelogram{
    public static void main(String[] args) {
        Figure linkParallelogram = new Parallelogram(10, 7);
        linkParallelogram.calculateSquare();
        linkParallelogram.print();
    }
}