package board;

/**
 * Создать метод в классе, который принимает строку и символ,которые нужно найти и возвращает
 * сколько раз этот символ встречается в строке.
 */
public class Print {
    public int end(String text, char symbol) {

        int z = 0;

        for (int j = 0; j < text.length(); j++) {
            if (text.charAt(j) == symbol) {
                z++;
            }
        }
        return z;
    }
}

class TestClass {
    public static void main(String[] args) {
        Print reference = new Print();
        int searchedSymbol = reference.end("Hello my little young friend", 'l');
        System.out.println(searchedSymbol);
    }
}