package meeting;

import java.util.Scanner;

/**
 * Создать приложение, позволяющие людям знакомиться.
 * Приложение должно позволять:
 * 1) Регистрироваться человеку старше 18 лет.
 * 2) После регистрации выводить список подходящих мужчин/ женщин для этого человека по возрасту.
 * 3) Просматривать зарегистрировавшихся людей. Для мужчин выводить только женщин и наоборот.
 * 4) Просматривать анкету отдельного человека(поиск по имени и фамилии)
 * 5) Организовать "умный поиск". Пользователь вводит требования(город, пол, возраст, количество детей) и  выводить людей,
 * которые соответствуют требованиям.
 */
public class Person {
    private String name;
    private String surname;
    private String city;
    private Gender gender;
    private int amountOfChildren;
    private int age;

    public Person(String name, String surname, String city, Gender gender, int amountOfChildren, int age) {
        this.name = name;
        this.surname = surname;
        this.city = city;
        this.gender = gender;
        this.amountOfChildren = amountOfChildren;
        if (age >= 18) {
            this.age = age;
        } else {
            System.out.println("You are very young.You must study.But,if your parents don't see,you can do it...");  ///break ??
        }
    }

    public int getAge() {
        return age;
    }

    public Gender getGender() {
        return gender;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getCity() {
        return city;
    }
}

class PersonService {
    private Person[] persons;

    public PersonService() {
        this.persons = new Person[10];
    }

    public void addPerson(Person person) {
        for (int i = 0; i < persons.length; i++) {
            if (persons[i] == null) {
                persons[i] = person;
                break;
            }
        }
    }

    public Person[] getPersons() {
        return persons;
    }

    public void getSuitablePerson(Person people) {
        for (Person person : persons) {
            if (people.getAge() > 18 && (person.getAge() - people.getAge() < 10) &&
                    people.getGender() != person.getGender()) {
                System.out.println(person);
            }
        }
    }

    public void getInformationAboutWoman(Person person) {
        for (Person object : persons) {
            if (object.getGender() == Gender.MAN &&
                    person.getGender() != object.getGender()) {
                System.out.println(object);
            }
        }
    }

    public void getInformationAboutMan(Person person) {
        for (Person object : persons) {
            if (person.getGender() == Gender.WOMAN &&
                    person.getGender() != object.getGender()) {
                System.out.println(object);
            }
        }
    }

    public void getPerson(Person nameAndSurname) {
        for (Person humans : persons) {
            if (nameAndSurname.getName().equals(humans.getName()) ||
                    nameAndSurname.getSurname().equals(humans.getSurname())) {
                System.out.println(humans);
            }
        }
    }
}

enum Gender {
    MAN, WOMAN;
}

class Test {
    public static void main(String[] args) {


        Person firstPerson  = new Person("Olga",  "Rybak",   "Kyiv",   Gender.WOMAN, 1, 32);

        Person secondPerson = new Person("Alex",  "Novik",   "Dnipro", Gender.MAN, 2, 35);

        Person thardPerson  = new Person("Marina","Harchenko","Zhitomyr", Gender.WOMAN, 0, 20);
        Person fourthPerson = new Person("Max",   "Bush",    "Dnipro", Gender.MAN, 1, 25);
        Person fifthPerson  = new Person("Nataly","Portman", "Kyiv", Gender.WOMAN, 0, 17);

        PersonService service = new PersonService();

        service.addPerson(firstPerson);
        service.addPerson(secondPerson);
        service.addPerson(thardPerson);
        service.addPerson(fourthPerson);
        service.addPerson(fifthPerson);

        Person[] arrayOfPeople = service.getPersons();

        Scanner search = new Scanner(System.in);

        System.out.println("Enter the city");

        String city = search.nextLine();

        for (Person cityParameters : arrayOfPeople) {
            if (city.equals(cityParameters.getCity())) {
                System.out.println(cityParameters.toString());
            }
        }
    }

}


