package array_try_again;

import java.util.Random;
import java.util.Scanner;

/**
 * � ��������� ������� ����� ����� ����������, ������� ��� � ��� ����������� ������� �� ��������� X,
 * ������ ����� ������ 23.
 * ��� �� ����������?
 * ��� ��� 80 ������������ ������ ������� � bound �
 * � bound? ��� �������������� ��� ���������� �������� �������� � ������, �����?
 * �� ���� ��� �� �����������? ������ bound �� ����
 * �������, �������
 */
public class ArrayTwo {
    public static void main(String[] args) {

        int[][] array = new int[4][6];

        System.out.println("Enter a SCANNER what you search");

        Scanner scanner = new Scanner(System.in);

        int searchedNumber = scanner.nextInt();

        int result = 0;

        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                array[i][j] = random.nextInt(80);

                if (array[i][j] == searchedNumber) {
                    result++;
                }

                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }

        String text = "There are " + result + " symbols of " + searchedNumber + " numbers in this list";

        if (result == 0) {
            text = "There is not " + searchedNumber + " SCANNER in this list";
        }

        System.out.println(text);
    }
}
