package baseOfOfficers;

/**
 * Создать базу данных сотрудников.
 * Каждый сотрудник принадлежит своему отделу и имеет определенную зарплату.
 * Зарплата сотруднику рассчитывается след. образом:
 * 1) Для управленца количество раб.дней * 1000
 * 2) Для сотрудника количество раб.дней * 100
 * По запросу выводить:
 * - данные сотрудников, зарплата которых меньше 500 гривень
 * - данные сотрудников  из определенного отдела
 * - данные сотрудников, которые проработали больше 10, но меньше 20 раб. дней
 * из одного и из разных отделов
 * - данные сотрудников, которые работают в одном отделе и имеют зарплату > 700 гривень
 * - данные сотрудников, которые работают в разных отделах и имеют одинаковые имена
 * <p>
 * Управленцев и менеджеров хранить в одном массиве.
 * Данные об отделе хранить в отдельном массиве.Создать массив.
 */

/*
* 1) Создать класс Department(название отдела)
* 2) В классе Human поле типа Department
* 3) Удалить класс DepartmentArray и перенести логику в класс HumanService
* 4) Удалить enum TeamLeader  и заменить полем типа boolean в классе Human
* */
public class Human {
    private int salary;
    private int timeOfWork;
    private Department department;
    private OficcersInformation human;
    private Gender genom;
    private boolean manager = true;
    private boolean teamLider = true;


    public Human(int timeOfWork,
                 Department department, OficcersInformation human,
                 Gender genom, boolean depart) {
        int money = timeOfWork * 100;

        if (depart == true) {
            money = money * 10;
        }

        this.salary = money;
        this.timeOfWork = timeOfWork;
        this.department = department;
        this.human = human;
        this.genom = genom;
        this.manager = manager;
        this.teamLider = teamLider;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public Department getDepartment() {
        return department;
    }

    public int getTimeOfWork() {
        return timeOfWork;
    }

    public OficcersInformation getHuman() {
        return human;
    }

//    Human foo = new Human();

//    String nameOfPeople = foo.getHuman().getName();

    public String getNameOfficcer() {
        return human.getName();
    }

//    create method for printing info about to console
}

class OficcersInformation {
    private String name;
    private String surname;
    private int age;

    public OficcersInformation(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public String getName() {
        return name;
    }
}

class HumanService {
    private Human[] humans;
    private Human[] departmentArray;

    public HumanService() {
        this.humans = new Human[20];//nulls
//        humans[0]= new Human()
    }

    public void addHuman(Human human) {
        for (int i = 0; i < humans.length; i++) {
            Human tempHuman = humans[i];
//            int temp = numbers[i];
            if (tempHuman == null) {
                humans[i] = human;
                break;
            }
        }
    }

    public Human[] getHumans() {
        return humans;
    }

    public void printWorkersBySalary() {
        for (Human human : humans) {
            if (human.getSalary() < 500) {
                System.out.println(human);
            }
        }
    }

    public void printHumansFromSpecificDepartment(String nameDepartment) {
        for (Human human : humans) {
            if (human.getDepartment().getName().equals(nameDepartment)) {
                System.out.println(human);
            }
        }
    }

    public void printTimeOfWork() {  ///human human[]
        for (Human human : humans) {
            if (human.getTimeOfWork() > 10 && human.getTimeOfWork() < 20) {
                System.out.println(human);
            }
        }
    }

    public void printTimeOfJob(String nameDepartment) {
        for (Human human : humans) {
            if (human.getTimeOfWork() > 10 && human.getTimeOfWork() < 20 &&
                    human.getDepartment().getName().equals(nameDepartment)) {
                System.out.println(nameDepartment + ":" + human);
            }
        }
    }

    public void printInformationAboutHumans(String department) {
        for (Human human : humans) {
            if (human.getSalary() > 700 && human.getDepartment().getName().equals(department)) {
                System.out.println(human);
            }
        }
    }

    public void departmentArray() {
        if (humans[0].getDepartment().getName().equals("manager") ||
                humans[0].getDepartment().getName().equals("teamLider")) {
                departmentArray[0] = humans[0];
        }
    }

    public void departmentAndNameOfficers() {
        for (int i = 0; i < humans.length; i++) {
            for (int z = 0; z < humans.length; z++) {
                if (humans[i].getNameOfficcer().equals(humans[z].getNameOfficcer()) && i != z) {
                    for (Human human : humans) {
                        if (!humans[i].getDepartment().equals(humans[z].getDepartment())) {
                            System.out.println(humans[i]);
                        }
                    }

                    if (!humans[i].getNameOfficcer().equals(humans[z].getNameOfficcer()) && i != z) {
                        break;
                    }
                }
            }
        }
    }
}

class Department {
    private String name;

    public Department(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

enum Gender {
    WOMAN, MAN;
}

class Test {
    public static void main(String[] args) {

        HumanService service = new HumanService();

        Department linkOne = new Department("manager");

        OficcersInformation peopleOne = new OficcersInformation("Max", "Syper", 25);

        Human cooperatorOne = new Human(9, linkOne, peopleOne,
                Gender.MAN, true);

        service.addHuman(cooperatorOne);

        Department linkTwo = new Department("teamLider");

        OficcersInformation peopleTwo = new OficcersInformation("Olga", "Charming", 27);

        Human cooperatorTwo = new Human(18, linkTwo,
                peopleTwo, Gender.WOMAN, false);

        service.addHuman(cooperatorTwo);

        Department linkTree = new Department("secretary");

        OficcersInformation peopleThree = new OficcersInformation("Olga", "Best", 30);

        Human cooperatorThree = new Human(4, linkTree,
                peopleThree, Gender.WOMAN, false);

        service.addHuman(cooperatorThree);

        Department linkFour = new Department("manager");

        OficcersInformation peopleFour = new OficcersInformation("Alex", "Petrov", 40);

        Human cooperatorFour = new Human(12, linkFour, peopleFour,
                Gender.MAN, true);

        service.addHuman(cooperatorFour);

    }
}