package library;

/**
 * Создать приложение библиотека с пользователями администратор и посетители.
 * Администратор может добавлять книги либо удалять книги.
 * Книгу можно выдавать посетителю если она не взята.
 * Для посетителя и администратора есть права,
 * посетитель не может добавлять книги,он может их только брать
 * Добавить возможность пароля.
 * Посетитель и администратор могут просматривать список книг.
 * Книги добавлять и хранить только по жанрам.
 * Библиотека работает только с 8-ми утра до 5-ти вечера.(системное время)
 * Cоздать меню. Применить наследование.
 * Сделать в отдельной ветке
 */
public class Book {
    private Genre genre;
    private String name;
    private int age;
    //add boolean variable for take/untake state

    public Book(Genre genre, String name, int age) {
        this.genre = genre;
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }
}

class Library {
    public static final int COUNT_ENTITIES = 20;
    private Book[] books;
    private Person[] visitors;


    public Library(Person admin) {
        this.books = new Book[COUNT_ENTITIES];
        this.visitors = new Person[COUNT_ENTITIES];
        visitors[0] = admin;
    }

    public void addBookToCatalog(Book bookCatalog) {
        for (int i = 0; i < books.length; i++) {
            if (books[i] == null) {
                books[i] = bookCatalog;
                break;
            }
        }
    }


    public void deleteBookFromCatalog(Person person, String bookName) {
        if (person.getRole() == Role.ADMIN) {
            for (int i = 0; i < books.length; i++) {
                /*if (books[i].getName().equals(bookName)) {
                    books[i] = null;
                } else {
                    System.out.println("Pleace,don't touch the book.Only administrator can give a literature");
                }*/

                if (!books[i].getName().equals(bookName)) {
                    System.out.println("Pleace,don't touch the book." +
                            "Only administrator can give a literature");
                    continue;
                }

                books[i] = null;

            }
        }
    }
}


enum Genre {
    DRAMA, COMEDY, DETECTIVE, NOVEL
}

class Start {
    public static void main(String[] args) {

        Book firstBook = new Book(Genre.DRAMA, "Love and dove", 1999);
        Book secondBook = new Book(Genre.COMEDY, "Stupit", 2010);
        Book thirdBook = new Book(Genre.NOVEL, "Tom", 2005);
        Book fourthBook = new Book(Genre.DETECTIVE, "Sherlok", 1980);
        Book fifthBook = new Book(Genre.COMEDY, "Life", 2004);
        Book sixthBook = new Book(Genre.NOVEL, "Dream", 2014);


        Person admin = new Person("Zoryana", Role.ADMIN);
        Person personTwo = new Person("Victor", Role.VISITOR);
        Person personFourth = new Person("Oleg", Role.VISITOR);

        Library link = new Library(admin);

        link.addBookToCatalog(firstBook);
        link.addBookToCatalog(secondBook);
        link.addBookToCatalog(thirdBook);
        link.addBookToCatalog(fourthBook);
        link.addBookToCatalog(fifthBook);
        link.addBookToCatalog(sixthBook);

//        link.deleteBookFromCatalog();

    }
}

