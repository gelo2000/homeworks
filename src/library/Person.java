package library;

/**
 * Created by onovak on 20.11.2017.
 */
public class Person {
    private String name;
    private Role role;

    public Person(String name, Role role) {
        this.name = name;
        this.role = role;
    }

    public Role getRole() {
        return role;
    }
}

enum Role {
    ADMIN, VISITOR
}