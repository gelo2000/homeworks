
package mathemetics;


/**
 * 1) Создать класс калькулятор. Создать методы для операций(+, -, \, %, *),
 * к-рые принимают два числа, а возвращают результат
 * <p>
 * 2) Отдельный класс Калькулятор. Создать два поля, методы для операций(+, -, \, %, *).
 * Методы производят действия над полями и поля менять перед каждым действием.
 * <p>
 */

public class Calculator {

    public int sum(int valueOne, int valueTwo) {
        int result = valueOne + valueTwo;
        return result;
    }

    public long difference(long differenceOne, long differenceTwo) {
        long result = differenceOne - differenceTwo;
        return result;
    }

    public int slash(int slashOne, int slashTwo) {
        int code = slashOne / slashTwo;
        return code;

    }

    public int percent(int centOne, int centTwo) {
        int centery = centOne % centTwo;
        return centery;
    }

    public int much(int veryMach, int veryLittle) {
        int last = veryMach * veryLittle;
        return last;
    }
}

class FirstCalculator {
    public static void main(String[] args) {
        Calculator link = new Calculator();
        link.sum(12, 2);
        link.difference(15, 5);
        link.slash(20, 4);
        link.percent(50, 5);
        link.much(33, 3);
        System.out.println(link.sum(12, 2));
        System.out.println(link.difference (15,5));
        System.out.println(link.slash (20,4));
        System.out.println(link.percent(50,5));
        System.out.println(link.much(33,3));
    }
}
