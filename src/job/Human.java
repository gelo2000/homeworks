package job;

/*
Создать класс Human c полями имя, пол(использовать Enum) фамилия, возраст, должность.
        Для должности создать отдельный класс с полем название должности.
        Создать несколько людей с одинаковой и разными должностями.
        Создать методы позволяющие:
         - вывести данные о человеке.
         - проверять должности на одинаковость у двух людей.
         - проверять однофамильцев у двух людей.
         */

public class Human {
    private String name;
    private String surname;
    private int age;
    private Profession profession;
    private Gender gender;


    public Human(String name, String surname, int age, Profession profession, Gender gender) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.profession = profession;
        this.gender = gender;

    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public Profession getProffesion() {
        return profession;
    }

    public Gender getGender() {
        return gender;
    }

    public boolean compareProffesion(Human human) {
        boolean isEqualsProfession =
                profession.getTitle().equals(human.getProffesion().getTitle());
        return isEqualsProfession;
    }

    public String getAllInformation() {
        return name        + "\t" +
                surname    + "\t" +
                age        + "\t" +
                profession + "\t" +
                gender;
    }
}


class Profession {
    private String title;

    public Profession(String prof) {

        this.title = prof;
    }

    public String getTitle() {
        return title;
    }

    public String toString() {
        return title;
    }

    /* public void setTitle(String title) {
    this.title = title;
    }*/
}

enum Gender {
    MAN, WOMAN
}


class TestClass {
    public static void main(String[] args) {
        String job = "soldier";
        Profession work = new Profession(job);
        Human humanOne = new Human("Diana", "Petrovna", 25, work, Gender.WOMAN);
        //      String one = humanOne.getName();
        String jobTwo = "doctor";
        Profession workTwo = new Profession(jobTwo);
        Human humanTwo = new Human("Vova", "Smirnov", 38, workTwo, Gender.MAN);
        System.out.println(humanOne.compareProffesion(humanTwo));

//      System.out.println(one);
//      seller.setJob("Foo");
//      System.out.println(humanOne.getNameOfJob().getJob());
        String one = humanOne.getAllInformation();
        String two = humanTwo.getAllInformation();
        System.out.println(one);
        System.out.println(two);

    }
}