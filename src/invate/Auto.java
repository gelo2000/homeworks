package invate;

/**
 * Подсчитать сколько раз в слове "Hello my young friend" встречается символ 'o'?
 */
public class Auto {
    public static void main(String[] args) {
        String array = " Hello my young friend";
        int count = 0;
        for (int j = 0; j < array.length(); j++) {
            if (array.charAt(j) == 'o') {
                count++;
            }
        }
        System.out.println("Symbol 'o' appears = " + count);
    }
}
