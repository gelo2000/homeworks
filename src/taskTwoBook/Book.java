package taskTwoBook;

/**
 * Created by oleg on 22.07.17.
 */
public class Book implements Comparable{
    private String name;
    private String author;
    private int year;
    private int pageAmount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Book(String name, String author, int year, int pageAmount) {
        this.name = name;
        this.author = author;
        this.year = year;
        this.pageAmount = pageAmount;
    }

    public int getPageAmount() {
        return pageAmount;
    }

    public void setPageAmount(int pageAmount) {
        this.pageAmount = pageAmount;
    }

    @Override
    public String toString() {
        String resultStr = "";

        resultStr += this.getName() + "; " + this.getAuthor() + "; " + this.getYear() + "; " + this.getPageAmount();

        return resultStr;
    }

    @Override
    public int compareTo(Object obj) {
        System.out.println(this);

        Book otherBook = (Book) obj;

        System.out.println(otherBook);
        System.out.println(this.name.compareTo(otherBook.getName()));

        return this.name.compareTo(otherBook.getName());
    }
}
