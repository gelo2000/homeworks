package taskTwoBook;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by oleg on 22.07.17.
 */
public class BookShelf {
    List<Book> books;

    public BookShelf() {
        this.books = new ArrayList<Book>();
    }

    public List<Book> getBooks() {
        return this.books;
    }

    public void addBook(Book book) {
        this.books.add(book);
    }

    public void addBooks(Book...books) {
        for (Book book : books) {
            this.addBook(book);
        }
    }
    public void sort() {
        //this.books.so
    }

    public String toString() {
        String result = "";

        for (Book book : this.books) {
            result += book.toString() + "\n";
        }

        return result;
    }
}
