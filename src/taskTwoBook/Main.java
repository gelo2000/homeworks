package taskTwoBook;

import java.util.Arrays;

/**
 * Created by oleg on 22.07.17.
 */
public class Main {
    public static void main(String[] args) {
        Book myBook1 = new Book("Book1", "Author1", 1991, 200);
        Book myBook2 = new Book("Book2", "Author2", 1991, 200);

        BookShelf myBookShelf = new BookShelf();
        myBookShelf.addBooks(myBook1, myBook2);
        System.out.println(myBookShelf);
        //Arrays.sort();
        System.out.println(myBookShelf);

    }
}
