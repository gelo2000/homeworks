package inheritance_and_interface;

public interface Writable {
    void write(String text);
}
