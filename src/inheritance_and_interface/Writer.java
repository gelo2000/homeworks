package inheritance_and_interface;

/*Создать абстрактный класс родителя Writer c методом modifyText, который реализует интерфейс Writable c абстрактными методом write.
  Создать потомка, который будет писать данные в текстовый файл Poem.txt.
  Данные вводит юзер с консоли.
  К  данным введенным юзером, в методе modifyText добавлять строку "I'm ready for writting to file".

  Создать абстрактный класс родителя Reader c методом modifyText, который реализует интерфейс Readable c абстрактным методом read.
   Создать потомка, который будет читать данные из текстового файла Poem.txt и вывести данные в консоль.
   Перед выводом в консоль в методе modifyText заменить строку "I'm ready for writting to file" на "I'm from file".

   Use only Java 7, 8, class Files*/


import java.io.IOException;
import java.nio.file.*;
import java.util.Scanner;

public abstract class Writer implements Writable {

    public String modifyText(String text) {                     // в методе psvm согласно порядку выполняются действия
        return text.concat(" I'm ready for writting to file");  // 1. Заходим в объект класса WriterPoem
    }                                                           // 2. Выполняем метод сканнер
}                                                               // 3. Выполняем метод write
                                                                // 4. Но где вход в класс Writer и выполнение метода modifyText?

//во-первых, ты не входишь в обьект, это неправильно. Ты создаешь обьект. В класс ты тоже не входишь. Тебе это не надо. Ты вызываешь методы обьекта какого-то класса. точнее не так
//ты вызываешь методы класса, на обьекте этого класса, который ты создал. А обьект в простом понимании это совокупность всех полей.
// то есть ты вызываешь методы класса, которые берут данные у обьекта.
//проще говоря, ты хочешь посчитать вес и зарплату и стоимость одежды какого-то человека. Обьект человек хранит все данные( вес, цвет волос и глаз, количество одежды и место работы)
//исходя из данных каждого обьекта(конкретного человека) ты получаешь ответы на свои вопросы, но конкретно по этому человеку(обьекту)
// хорошо,я вызываю методы объекта,но. ......нет ты вызываешь методы класса,методы класса, но если посмотреть в psvm метод modifyText не вызывался но программа работает....
// давай проясним хотя бы одно: ты вызываешь методы класса, но на обьекте(конкретном) А обьект хранит все данные, да выполнения этих методов Это понятно? Ты здесь?да,кумекаю
//где psvm?

class WriterPoem extends Writer {

    @Override
    public void write(String text) {

        text = modifyText(text);
        ;// вот вызов методадумаю над этим...  здесь метод модифай возвращает сюда строку, я привык что есть ссылка и точка  какая точка? link.moааааd ifyпонял
        //смотри. есть методы, которые возвращают что-нибудь в то место, откуда были вызваны, а есть те, которые ничего не возвращают, а что-то выполняют
        //те, что возвращают что-то имеют тип
        //public String вернет строку. А ее можно присвоить переменной стринговой, можно вывести в консоль
        //System.out.println(modifyText());, а можно написать String text = modifyText(); понимаешь? то есть этод метод вернул информацию?да в системаут,хорошо спасибо,я еще сам подумаю,не буду тебя задерживать
        //пиши если что,ок


        Path path = FileSystems.getDefault().getPath("/home/oleg/Example", "Poem.txt");

        try {
            Files.write(path, text.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class TestClass {
    public static void main(String[] args) {

        WriterPoem link = new WriterPoem();

        Scanner printText = new Scanner(System.in);
        System.out.println("Pleace, enter the text");
        String textOne = printText.nextLine();

        link.write(textOne);

    }
}