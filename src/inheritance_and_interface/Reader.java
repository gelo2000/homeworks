package inheritance_and_interface;

/*Создать абстрактный класс родителя Reader c методом modifyText, который реализует интерфейс Readable c абстрактным методом read.
        Создать потомка, который будет читать данные из текстового файла Poem.txt и вывести данные в консоль.
        Перед выводом в консоль в методе modifyText заменить строку "I'm ready for writting to file" на "I'm from file".*/

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.*;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

public abstract class Reader implements Readable {

    public String modifyText(String txt) {
        return txt.replace("I'm ready for writting to file", "Im reading from file");
    }
}

class TextReader extends Reader {

    @Override
    public void read() throws IOException {

        Path path = FileSystems.getDefault().getPath("/home/oleg/Example", "Poem.txt");

        BufferedReader bufferedReader = Files.newBufferedReader(path);

        String text = bufferedReader.readLine();

        String lastVariantOfText = modifyText(text);

        System.out.println(lastVariantOfText);
    }
}

class Test {
    public static void main(String[] args) throws IOException {

        TextReader link = new TextReader();

        link.read();
    }
}