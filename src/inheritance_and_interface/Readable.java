package inheritance_and_interface;

import java.io.BufferedReader;
import java.io.IOException;

public interface Readable {
    public void read() throws IOException;
}
