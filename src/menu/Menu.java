package menu;

import java.util.Scanner;

public class Menu {
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            menu();
            int key = scanner.nextInt();
            try {
                switch (key) {
                    case 1: {
                        break;
                    }
                    case 2: {
                        break;

                    }
                    case 3: {
                        break;
                    }
                    case 9: {
                        return;
                    }

                    default:
                        return;
                }
            } catch (Exception e) {
                System.out.println("Exception" + e);
            }
        }
    }

    public static void menu() {
        System.out.println("1) Выводить родословное дерево конкретного человека.\n" +
                "2) Показывать прямых родственников.\n" +
                "3) Вычислять статистику по всему дереву: количество живых.\n" +
                "4) Вычислять статистику по всему дереву: мужчин/женщин.\n" +
                "5) Вычислять статистику по всему дереву: среднее количество детей.\n" +
                "6) Вычислять статистику по всему дереву: среднюю продолжительность жизни.\n" +
                "7) Показывать прямых родственников с братьями и сёстрами, все родственники.\n" +
                "8) Показывать степень родства двух людей.\n" +
                "9) Exit.");
    }
}

