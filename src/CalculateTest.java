import junitparams.JUnitParamsRunner;
import junitparams.Parameters;                   //где это брать и что это такое?
import org.junit.*;                              // вопросики по теории между делом почему так а не так...?
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;

//@RunWith(Parameterized.class)
@RunWith(JUnitParamsRunner.class)
public class CalculateTest {
    public static final CalculatorMethod METHOD = new CalculatorMethod();

    @Before
    public void firstLine(){
        System.out.println("Start\n"+
                              " ");
    }

    @Test
    @Parameters({"100|25","60|3","50|10"})
    public void bestTestOne(int value1,int value2){

        int result = value1/value2;

        Assert.assertEquals("Best test",result,METHOD.division(value1, value2));

    }

    @Test
    @Parameters({"10|2","6|3","7|1"})
    public void bestTestTwo(int value1,int value2){

        int resultOne = value1+value2;
        int resulTwo = value1-value2;                                                        //так тоже можно?
        int resulThree = value1*value2;
        int resulFour = value1/value2;

        Assert.assertEquals("Best test",resultOne,METHOD.amount(value1, value2));
        Assert.assertEquals("Best tes two",resulTwo,METHOD.difference(value1, value2));
        Assert.assertEquals("Best tes two",resulThree,METHOD.multiply(value1, value2));
        Assert.assertEquals("Best tes two",resulFour,METHOD.division(value1, value2));
    }

    @After
    public void lastLine(){
        System.out.println("Finish\n"+
                              " ");
    }






 /*   @Parameterized.Parameter(value = 3)
    public int argOne;

    @Parameterized.Parameter
    public int argTwo;

    @Parameterized.Parameter(value = 1)
    public int argThree;

    @Parameterized.Parameter(value = 2)
    public int argFour;


    @Parameterized.Parameters
    public static Collection<Object[]> data() {                    //Collection<Object[]> что это?тип объектов массива?
        Object[][] data = new Object[][]{{2, 7, 0, 5}, {3, 10, 6, 1}};
        return Arrays.asList(data);
    }

    @Test
    public void parameterizedMethodTest() {
        CalculatorMethod test = new CalculatorMethod();
        int result = argOne + argThree;
        Assert.assertEquals("Result", result, test.amount(argOne, argThree));
    }

    @Test
    public void parameterziedMethodTestTwo(){
        int result = argTwo-argFour;
        Assert.assertEquals("Final result",result,METHOD.difference(argTwo,argFour));
    }

    @Test
    public void parameterziedMethodTestThree(){
        int result = argTwo/argTwo;
        Assert.assertEquals("Show result",result,METHOD.division(argTwo,argTwo));
    }

    @Test
    public void parameterziedMethodTestFour(){
        int result=argOne*argOne  ;
        Assert.assertEquals("Parameterzied",result,METHOD.multiply(argOne,argOne));

    }*/


  /*  @BeforeClass
    public static void testBeforeClass() {
        System.out.println("Start\n" +
                " ");
    }
    @Before
    public void showMassageBeforeEachTest(){
        System.out.println("Start test your methods");
    }
    @Test
    public void testAmountMethod(){
        Assert.assertEquals("Result " , 7, METHOD.amount(5, 2)); //  не понравилось?
    }

    @Ignore
    @Test
    public void testDifferenceMethod(){
        CalculatorMethod link = new CalculatorMethod();
        Assert.assertEquals("Result",2,link.difference(5,3));
    }

    @Test
    public void testDivisionMethod(){
        Assert.assertEquals("Result",2,METHOD.division(10,5));
    }

    @Test
    public void testMultiplyMethod(){
        CalculatorMethod link = new CalculatorMethod();
        Assert.assertEquals("Result",10,link.multiply(5,2));
    }

    @After
    public void showMassageAfterEachTest(){
        System.out.println("Finish test your methods\n"+
                            " ");
    }
        @AfterClass
        public static void testAfterClass() {
            System.out.println("Finish\n" +
                    " ");
        }*/
}
