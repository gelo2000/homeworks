package biblioteka;

/**
 * Created by onovak on 20.11.2017.
 */
public class User {
    private String name;

    private Role role;
    //add login, password fields

    public User(String name, Role role) {
        this.name = name;
        this.role = role;
    }

    public Role getRole() {
        return role;
    }
}

class UserService {
    private User[] users;

    public UserService() {
        users = new User[5];
        users[0] = new User("Zoryana", Role.ADMIN);

        users[1] = new User("Victor", Role.VISITOR);

        users[2] = new User("Oleg", Role.VISITOR);
    }

    //add method for adding users
}

enum Role {
    ADMIN("Admin"),
    VISITOR("Visitor");

    private String shortName;

    Role(String shortName) {
        this.shortName = shortName;
    }

    public String getShortName() {
        return shortName;
    }
}