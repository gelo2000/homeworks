package biblioteka;

import java.util.Arrays;

/**
 * Создать приложение библиотека с пользователями администратор и посетители.
 * 1.Пароли для окон меню админа и пользователя.
 * 2.Cоздать меню для админа и пользователя.
 * Администратор может добавлять книги либо удалять книги.
 * Книгу можно выдавать посетителю если она не взята.?
 * Для посетителя и администратора есть права,посетитель не может добавлять книги,он может их только брать?
 * Посетитель и администратор могут просматривать список книг.
 * Книги добавлять и хранить только по жанрам.
 * Библиотека работает только с 8-ми утра до 5-ти вечера.(системное время)
 * Сделать в отдельной ветке
 */
public class Book {
    private Genre genre;
    private String name;
    private int age;
    private boolean taken;

    public Book(Genre genre, String name, int age) {
        this.genre = genre;
        this.name = name;
        this.age = age;
        this.taken = false;
    }

    public String getName() {
        return name;
    }

    public boolean isTaken() {
        return taken;
    }


    public String toString() {
        return genre + " " + name + " " + age + ";";
    }

/*    @Override
    public String toString() {
        return  "genre=" + genre +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }*/
}

class BookService {                           // класс не виден но объект создается?
    private Book[] books;
//    private UserService userService;

    BookService() {
        this.books = new Book[10];

        Book firstBook = new Book(Genre.DRAMA, "Love and dove", 1999);
        Book secondBook = new Book(Genre.COMEDY, "Stupit", 2010);
        Book thirdBook = new Book(Genre.NOVEL, "Tom", 2005);
        Book fourthBook = new Book(Genre.DETECTIVE, "Sherlok", 1980);
        Book fifthBook = new Book(Genre.COMEDY, "Life", 2004);
        Book sixthBook = new Book(Genre.NOVEL, "Dream", 2014);


        addBook(firstBook);
        addBook(secondBook);
        addBook(thirdBook);
        addBook(fourthBook);
        addBook(fifthBook);
        addBook(sixthBook);
    }

    void addBook(Book book) {
        for (int i = 0; i < books.length; i++) {
            if (books[i] == null) {
                books[i] = book;
                break;
            }
        }

    }


    void showListBooks() {
        for (Book book : books) {
            if (book != null) {
                System.out.println(book);     // убрать null? создать свой метод взяв за основу toString?
            }
        }
    }

    String isTakenBook(String nameOfBook) {

        String taken = "Take";

        for (Book book : books) {
            if (book.getName().equalsIgnoreCase(nameOfBook)) {     //пустое тело?
                taken = "Free";
                break;
            }
        }

        return taken;
    }


    void removeBook(String bookName) {
        for (Book book : books) {
            if (book.getName().equalsIgnoreCase(bookName)) {
                book = null;
            }
        }

        showListBooks();
    }

    void returnBook(Genre genre, String name, int age) {
        Book newBook = new Book(genre, name, age);

        for (int i = 0; i < books.length; i++) {//foreach
            if (books[i] == null) {
                books[i] = newBook;
                break;
            }
        }

        showListBooks();
    }
}

class AdminSecurity {
    private int adminMenuKey;

    public AdminSecurity(int adminMenuKey) {
        this.adminMenuKey = adminMenuKey;
    }
}

class UserSecurity {
    private int userMenuKey;

    public UserSecurity(int userMenuKey) {
        this.userMenuKey = userMenuKey;
    }

    public int getUserMenuKey() {
        return userMenuKey;
    }

}

class KeyService {
    public int getFirstKey(UserSecurity key) {
        return key.getUserMenuKey();
    }
}

enum Genre {
    DRAMA("Drama"),
    COMEDY("Comedy"),
    DETECTIVE("Detective"),
    NOVEL("Novel");   // перебросить "мелкие" названия?

    private String nameShort;

    Genre(String nameShort) {
        this.nameShort = nameShort;
    }

    public String getNameShort() {
        return nameShort;
    }
}
