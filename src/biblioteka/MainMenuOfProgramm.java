package biblioteka;

import java.util.Scanner;

public class MainMenuOfProgramm {

    private final static Scanner SCANNER = new Scanner(System.in);

    private static BookService bookService = new BookService();

    private static KeyService password = new KeyService();



    private static void startMainMenuOfProgramm(String role) {
//        SCANNER = null;

        if (role.equalsIgnoreCase(Role.ADMIN.getShortName())) {
            enterInAdminMenu();
        } else {
            enterInVisitorMenu();                                                 //Visitor реестр букв?
        }
    }

    private static void enterInAdminMenu() {

        while (true) {
            menuAdmin();

            int key = SCANNER.nextInt();

            try {
                switch (key) {
                    case 1: {
                        bookService.showListBooks();
                    }
                    case 2: {
                    }
                    case 3: {

                    }
                }
            } catch (Exception e) {
                System.out.println("Exception" + e);
            }
        }
    }

    private static void enterInVisitorMenu() {

        while (true) {
            menuUser();

            int key = SCANNER.nextInt();

            try {
                switch (key) {
                    case 1: {
                        bookService.showListBooks();
                        break;
                    }
                    case 2: {

                        System.out.println("Do you want Take or Remove a book?");
                        String link = SCANNER.next();

                        if (link.equals("Take")) {

                            System.out.println("Pleace, enter the name of book,what do you want to take");
                            String nameBook = SCANNER.next();
//                            if(bookService.availabilityOfBook(nameBook).equalsIgnoreCase("Take")){
                            bookService.removeBook(nameBook);
                           /* } else{
                                System.out.println("Sorry, but this book absents in this book-list");
                            } */
                        } else {

                            String bookName = getInfoFromUser("Pleace, enter the name of book,what do you want to return\n" +
                                    "Love and dove, Stupit, Tom, Sherlok, Life, Dream");


                            int bookAge = Integer.parseInt(getInfoFromUser("Pleace, enter the age of book, " +
                                                                                        "what do you want to return"));

                            System.out.println("Pleace, make your choice.What genre of the book, do you want to return?\n" +
                                    "1)DRAMA, 2)COMEDY, 3)DETECTIVE or 4)NOVEL");
                            int keys = SCANNER.nextInt();

                            switch (keys) {
                                case 1: {
                                    bookService.returnBook(Genre.DRAMA, bookName, bookAge);
                                    break;
                                }
                                case 2: {
                                    bookService.returnBook(Genre.COMEDY, bookName, bookAge);
                                    break;
                                }
                                case 3: {
                                    bookService.returnBook(Genre.DETECTIVE, bookName, bookAge);
                                    break;
                                }
                                case 4: {
                                    bookService.returnBook(Genre.NOVEL, bookName, bookAge);
                                    break;
                                }
                            }
                        }
                        break;
                    }
                    case 3: {
                        System.out.println("Do you want enter a 'New password' or 'Change' old password?");
                        String newPassword = SCANNER.next();

                        if (newPassword.equalsIgnoreCase("New password")) {  //создать и сохранить пароль,редактировать его
                            System.out.println("Enter your password");
                            int parol = SCANNER.nextInt();

                            UserSecurity userPassword = new UserSecurity(parol);
                            if (password.getFirstKey(userPassword) > 0) {
                            }
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Exception" + e);
            }
        }
    }

    private static String getInfoFromUser(String message) {
        System.out.println(message);
        return SCANNER.next();
    }

    private static void menuAdmin() {

        System.out.println("1) List of book.\n" +
                "2) Add or remove book from the list.\n" +  //приблизительно как в User
                "3) Add and save a book in the list(by genre).\n" +  //genre? //приблизительно как в User
                "4) Add a password to enter in menu.\n" + // ???
                "5) Add a user to list of VISITORS.\n" +
                "6) System time.\n"
        );
    }

    private static void menuUser() {

        System.out.println("1) List of book.\n" +
                "2) Take or return a book from the list.You can take the book if it doesn't taken\n" +
                "3) Add a password to enter the menu.\n" // ???
        );
    }

    public static void main(String[] args) {

        String info = getInfoFromUser("What type of menu do you want to open, ADMIN or VISITOR ?");

        startMainMenuOfProgramm(info);

        //       Calendar calendar = Calendar.getInstance();
        //       calendar.get(Calendar.HOUR_OF_DAY);


    }
}

