package array;

import java.util.Random;

/**
 * Необходимо найти и вывести наибольший, наименьший элемент,
 * сумму всех элементов каждого столбца.
 */
public class Mobile {
    public static void main(String[] args) {
        Random random = new Random();

        int[][] array = new int[5][7];

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = random.nextInt(100);
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }


        for (int i = 0; i < array[4].length; i++) {
            int min = array[0][i];
            int max = array[0][i];
            int sum = 0;
            for (int j = 0; j < array.length; j++) {
                if (array[j][i] < min) {
                    min = array[j][i];
                }

                if (array[j][i] > max) {
                    max = array[j][i];
                }
                sum = sum + array[j][i];
            }

            System.out.println("Max = " + max + " \nMin = " + min);
            System.out.println(" Column sum" + " = " + sum);

        }
    }
}

