package bank;

import java.util.Scanner;

/**
 * Создать класс Банковский счет.
 * 1) Установить сумму для счета - 200$
 * Создать метод, который будет отнимать от счета сумму.
 * Если денег недостаточно, тогда написать в консоль - "Пополните счет".
 * Счет можно пополнить другим методом.
 */
public class BankScore {
    private int amount;

    public BankScore(int amount) {
        this.amount = amount;
    }

    public void checkBalance(int result) {//rename method's name and incoming parameter
        int tempAmmount = amount - result;

        String tempStr = "You balance is negative,you must replenish bank score by " +
                (-tempAmmount) + " dollars";

        if (tempAmmount >= 0) {
            tempStr = "Your balance is " + tempAmmount;
        }

        System.out.println(tempStr);
    }
}

class TestClass {

    public static final Scanner SCANNER = new Scanner(System.in);

    private static int getSumFromUser(String message) {
        System.out.println(message);
        return SCANNER.nextInt();
    }

    public static void main(String[] args) {

        int personalScore = getSumFromUser("Pleace, enter the sum of your score");

        BankScore link = new BankScore(personalScore);

        int result = getSumFromUser("Pleace, enter the sum what you want to pull off");

        link.checkBalance(result);
    }

}