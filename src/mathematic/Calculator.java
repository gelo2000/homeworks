package mathematic;

public class Calculator {
    private int valueOne;
    private int valueTwo;

    public Calculator(int valueOne, int valueTwo) {
        this.valueOne = valueOne;
        this.valueTwo = valueTwo;
    }

    public int sum() {
        int result = valueOne + valueTwo;
        return result;
        // return valueOne + valueTwo;
    }

    public void setValueOne(int valueOne) {
        this.valueOne = valueOne;
    }

    public void setValueTwo(int valueTwo) {
        this.valueTwo = valueTwo;
    }
}

class TestClass {
    public static void main(String[] args) {
        Calculator calc = new Calculator(5, 10);
        int answer = calc.sum();
        System.out.println(answer);
    }

}