package for_test;

import java.util.Scanner;

public class AskPassword {
    private static final Scanner SCANNER = new Scanner(System.in);

    void checkPassword(String key) {

        String password = getUserInfo("Enter password.");

        while (!password.equalsIgnoreCase(key)) {
            System.out.println("You have entered wrong password, try again.");
            password = getUserInfo("Enter password.");
        }

        System.out.println("You have entered correct password.");

    }

    private String getUserInfo(String message) {
        System.out.println(message);
        return SCANNER.nextLine();
    }

}
