package massiveone;

/**
 * Создать два массива из 30 чисел. Первый массив проинициализировать нечетными числами.
 * Проинициализировать второй массив элементами первого массива при условии,
 * что индекс элемента больше 4 и делится без  остатка на 5 и элемент больше 0,
 * но меньше 6 или больше 10, но меньше 20.
 * Если условие не выполняется оставить элемент массива без изменения.
 */
public class NewChoice {
    public static void main(String[] args) {

        int length = 30;

        int[] mass = new int[length];
        int[] array = new int[length];//0

        for (int i = 0; i < length; i++) {
            mass[i] = i * 2 + 1;
            System.out.print(mass[i] + "\t");
        }

        System.out.println();

        for (int i = 0; i < length; i++) {
            if (i > 4 && i % 5 == 0 &&
                    (mass[i] > 0 && mass[i] < 6 ||
                            mass[i] > 10 && mass[i] < 20)) {//true && false && true && false || true && true
                array[i] = mass[i];
            }

            System.out.print(array[i] + "\t");

        }
    }
}

