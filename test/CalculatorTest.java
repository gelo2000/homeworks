import for_test.Calculator;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;


//@RunWith(Parameterized.class)
@RunWith(JUnitParamsRunner.class)
public class CalculatorTest {

    int[] array;

    @Parameterized.Parameter(value = 1)
    public int valueOne;//1

    @Parameterized.Parameter
    public int valueTwo;//51

     @Parameterized.Parameter(value = 2)
    public int valueThree;//9

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{{51, 1, 9}, {2, 20, 3}};
        return Arrays.asList(data);
    }


    @BeforeClass
    public static void beforeAllTests() {
//        System.out.println("Before all tests");
    }

    @Before
    public void beforeEachTest() {
//        System.out.println("Before each test");

//        array = new int[5];
    }


    @Ignore
    @Test
    public void shouldReturnSumValues() {
        Calculator calculator = new Calculator();

        Assert.assertEquals("Should return 10", 10, calculator.sum(7, 3));
//        Assert.assertEquals(10, calculator.sum(7, 3));
    }

    @Test
    public void shouldReturnSumValuesParams() {
        Calculator calculator = new Calculator();

        int result = valueOne + valueTwo;

        Assert.assertEquals("Should return " + result, result, calculator.sum(valueOne, valueTwo));
//        Assert.assertEquals(10, calculator.sum(7, 3));
    }

    @Test
    @Parameters({"8 | 9", "7 | 4"})
    public void shouldReturnSumValuesJUnitParams(int value1, int value2) {
        Calculator calculator = new Calculator();

        int result = value1 + value2;

        Assert.assertEquals("Should return " + result, result, calculator.sum(value1, value2));
//        Assert.assertEquals(10, calculator.sum(7, 3));
    }

    @After
    public void afterEachTest() {
//        System.out.println("After each test");
    }

    @AfterClass
    public static void afterAllTests() {
//        System.out.println("After all tests");
    }

}
